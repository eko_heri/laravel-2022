@extends('layouts.global')

@section('title')
Home
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                {{-- <div class="card-header">Dashboard</div> --}}
                
                <div class="alert alert-success" role="alert">
                            Selamat datang, {{ Auth::user()->name }}
                </div>
                <div class="row">
                    <div class="col-md-11 pl-3 pt-4">
                        <div class="row pl-2">

                            <div class="col-lg-3 col-md-6 mb-2 col-sm-6">
                                <div class="card border-0 shadow-sm bg-danger text-light">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body">
                                                <h2 class="fw-bold">{{ $users }}</h2>
                                                <h6>Jumlah Users</h6>
                                            </div>
                                            <span class="oi oi-chat p-2 fs-9 text-danger-lighter"></span>
                                        </div>
                                    </div>
                                    <div class="card-footer border-0 text-center p-1 bg-danger-lighter">
                                        <a href="#" class="text-light">
                                            More info <span class="oi oi-arrow-circle-right"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 mb-2 col-sm-6">
                                <div class="card border-0 shadow-sm bg-secondary text-dark">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body">
                                                <h2 class="fw-bold">{{ $categories }}</h2>
                                                <h6>Kategori</h6>
                                            </div>
                                            <span class="oi oi-bar-chart p-2 fs-9 text-dark-lighter"></span>
                                        </div>
                                    </div>
                                    <div class="card-footer border-0 text-center p-1 bg-dark-lighter">
                                        <a href="#" class="text-secondary">
                                            More info <span class="oi oi-arrow-circle-right"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 mb-2 col-sm-6">
                                <div class="card border-0 shadow-sm bg-primary text-light">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body">
                                                <h2 class="fw-bold">0</h2>
                                                <h6>Purchase</h6>
                                            </div>
                                            <span class="oi oi-basket p-2 fs-9 text-indigo-lighter"></span>
                                        </div>
                                    </div>
                                    <div class="card-footer border-0 text-center p-1 bg-primary-lighter">
                                        <a href="#" class="text-light">
                                            More info <span class="oi oi-arrow-circle-right"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 mb-2 col-sm-6">
                                <div class="card border-0 shadow-sm bg-warning text-primary">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body">
                                                <h2 class="fw-bold">0</h2>
                                                <h6>Purchase</h6>
                                            </div>
                                            <span class="oi oi-basket p-2 fs-9 text-primary-lighter"></span>
                                        </div>
                                    </div>
                                    <div class="card-footer border-0 text-center p-1 bg-warning-lighter">
                                        <a href="#" class="text-primary">
                                            More info <span class="oi oi-arrow-circle-right"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{session('status')}}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
