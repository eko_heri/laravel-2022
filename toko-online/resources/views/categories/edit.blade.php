@extends('layouts.global')

@section('title') Edit Category
@endsection

@section('content')
<div class="col-md-8">
<form
    action="{{route('categories.update', [$category->id])}}" enctype="multipart/form-data" method="POST"
    class="bg-white shadow-sm p-3">
@csrf
<input type="hidden" value="PUT" name="_method">
</form>
</div>
@endsection